import { Component, OnInit } from "@angular/core";
import {
  PhoneBookService,
  IPhoneBookContact
} from "../common/phonebook.service";

@Component({
  selector: "app-add-contact",
  templateUrl: "./add-contact.component.html",
  styleUrls: ["./add-contact.component.css"]
})
export class AddContactComponent implements OnInit {
  constructor(private phoneBookService: PhoneBookService) {}
  fio: string = "";
  numberRoom: number;
  numberPhone: string = "";
  error: boolean = false;
  ngOnInit() {}

  addContact() {
    const contact: IPhoneBookContact = {
      id: this.phoneBookService.phoneBook.length + 1,
      fio: this.fio,
      numberRoom: this.numberRoom,
      numberPhone: this.numberPhone
    };
    console.log(this.fio, this.numberPhone);
    if (this.fio === "" || this.numberPhone === "") {
      this.error = true;
      setTimeout(() => {
        this.error = false;
      }, 2000);
    } else {
      this.phoneBookService.addContact(contact);
      this.fio = "";
      this.numberRoom = null;
      this.numberPhone = "";
    }
  }
}
