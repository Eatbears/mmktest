import { Component, OnInit } from "@angular/core";
import { PhoneBookService } from "../common/phonebook.service";

@Component({
  selector: "app-phonebook",
  templateUrl: "./phonebook.component.html",
  styleUrls: ["./phonebook.component.css"]
})
export class PhonebookComponent implements OnInit {
  constructor(private phoneBookService: PhoneBookService) {}

  phoneBook: Array<object>;
  loading: boolean = true;
  searchString: string = "";
  typeSort: string = "default";

  ngOnInit() {
    this.phoneBookService.fetchContacts().subscribe(response => {
      this.phoneBook = response;
      this.loading = false;
    });
  }

  removeContact(id: number) {
    this.phoneBookService.removeContact(id);
  }

  editContact(id: number) {
    this.phoneBookService.editContact(id);
  }
}
