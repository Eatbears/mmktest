import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { PhonebookComponent } from "./phonebook/phonebook.component";
import { AddContactComponent } from "./add-contact/add-contact.component";
import { EditContactComponent } from "./edit-contact/edit-contact.component";

import { PhoneBookFilterPipe } from "./common/phonebook.filter.pipe";
import { PhoneBookSorterPipe } from "./common/phonebook.sorter.pipe";
@NgModule({
  declarations: [
    AppComponent,
    PhonebookComponent,
    AddContactComponent,
    EditContactComponent,
    PhoneBookFilterPipe,
    PhoneBookSorterPipe
  ],
  imports: [BrowserModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
