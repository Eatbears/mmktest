import { Component, OnInit } from "@angular/core";
import {
  PhoneBookService,
  IPhoneBookContact
} from "../common/phonebook.service";

@Component({
  selector: "app-edit-contact",
  templateUrl: "./edit-contact.component.html",
  styleUrls: ["./edit-contact.component.css"]
})
export class EditContactComponent implements OnInit {
  constructor(private phoneBookService: PhoneBookService) {}

  editContact: any;
  changesDeclined: boolean = false; // для таблички о отмене
  changesAccepted: boolean = false; // для таблички о сохранении
  newFio: string = null;
  newNumberRoom: number = null;
  newNumberPhone: string = null;

  ngOnInit() {
    this.phoneBookService.currentContact.subscribe(contact => {
      this.editContact = contact;
    });
  }

  acceptChanges(id: number) {
    let newContactData: object = {
      fio: this.newFio,
      numberRoom: this.newNumberRoom,
      numberPhone: this.newNumberPhone
    };
    this.phoneBookService.saveChangingContact(newContactData, id);
    this.editContact = null;
    this.changesAccepted = true;
    setTimeout(() => {
      this.changesAccepted = false;
    }, 1000);
  }

  declineChanges() {
    this.editContact = null;
    this.phoneBookService.editContact(null);
    this.changesDeclined = true;
    setTimeout(() => {
      this.changesDeclined = false;
    }, 1000);
  }
}
