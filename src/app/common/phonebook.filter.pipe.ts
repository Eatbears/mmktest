import { Pipe, PipeTransform } from "@angular/core";
import { IPhoneBookContact } from "./phonebook.service";

@Pipe({
  name: "phoneBookFilter"
})
export class PhoneBookFilterPipe implements PipeTransform {
  transform(
    phoneBook: IPhoneBookContact[],
    search: string = ""
  ): IPhoneBookContact[] {
    if (!search.trim()) {
      return phoneBook;
    }
    if (isNaN(parseInt(search.charAt(0)))) {
      // проверка первого символа, если число, то поиск идет по номеру телефона, иначе по фио, сделал так, чтобы пользователь мог пользоваться только одной строкой поиска. Можно было
      return phoneBook.filter(contact => {
        return contact.fio.toLowerCase().indexOf(search.toLowerCase()) !== -1;
      });
    } else {
      return phoneBook.filter(contact => {
        return (
          contact.numberPhone.toLowerCase().indexOf(search.toLowerCase()) !== -1
        );
      });
    }
  }
}
