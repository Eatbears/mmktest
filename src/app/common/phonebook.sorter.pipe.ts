import { PipeTransform, Pipe } from "@angular/core";
import { IPhoneBookContact } from "./phonebook.service";

@Pipe({
  name: "phoneBookSorter"
})
export class PhoneBookSorterPipe implements PipeTransform {
  transform(
    phoneBook: IPhoneBookContact[],
    typeSort: string = ""
  ): IPhoneBookContact[] {
    if (typeSort === "default") {
      return phoneBook;
    }
    if (typeSort === "fio_asc") {
      // сортировка по фио возрастание
      return phoneBook.sort((a, b) =>
        a.fio > b.fio ? 1 : b.fio > a.fio ? -1 : 0
      );
    }
    if (typeSort === "fio_desc") {
      return phoneBook.sort((a, b) =>
        a.fio < b.fio ? 1 : b.fio < a.fio ? -1 : 0
      );
    }
    if (typeSort === "phone_desc") {
      return phoneBook.sort((a, b) =>
        a.numberPhone < b.numberPhone
          ? 1
          : b.numberPhone < a.numberPhone
          ? -1
          : 0
      );
    }
    if (typeSort === "phone_asc") {
      return phoneBook.sort((a, b) =>
        a.numberPhone > b.numberPhone
          ? 1
          : b.numberPhone > a.numberPhone
          ? -1
          : 0
      );
    }
  }
}
