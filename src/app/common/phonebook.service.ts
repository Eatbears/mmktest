import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { Observable, of, pipe } from "rxjs";
import { delay } from "rxjs/operators";

export interface IPhoneBookContact {
  id: number;
  fio: string;
  numberRoom: number;
  numberPhone: string; // сделал строкой, т.к. есть вероятность того, что человек запишет 8-999 или 8(999) и т.д.
}
@Injectable({ providedIn: "root" })
export class PhoneBookService {
  public phoneBook: IPhoneBookContact[] = [
    {
      id: 1,
      fio: "Пупкин Василий Иванович",
      numberRoom: 2,
      numberPhone: "8-999-12-31-434"
    },
    {
      id: 2,
      fio: "Петров Петр Петрович",
      numberRoom: 3,
      numberPhone: "22-20-22"
    },
    {
      id: 3,
      fio: "Семенов Семен Семенович",
      numberRoom: 3,
      numberPhone: "495-00-43"
    },
    {
      id: 4,
      fio: "Аваков Дмитрий Викторович",
      numberRoom: 2,
      numberPhone: "123-123123"
    }
  ];

  getContactIndex(id: number) {
    return this.phoneBook.findIndex(contact => contact.id === id);
  }

  private editingContact = new BehaviorSubject<object>(null); // использую его для установки первоначального значения, чтобы не отображать форму изменения пользователя(там проверка есть)
  currentContact = this.editingContact.asObservable();

  fetchContacts(): Observable<IPhoneBookContact[]> {
    return of(this.phoneBook).pipe(delay(1000)); // эмулирую загрузку с сервера
  }

  addContact(contact: IPhoneBookContact) {
    this.phoneBook.push(contact);
  }

  removeContact(id: number) {
    this.phoneBook.splice(this.getContactIndex(id), 1);
  }

  saveChangingContact(receivedContact: object, id: number) {
    Object.keys(receivedContact).forEach(
      key => receivedContact[key] == null && delete receivedContact[key] // В итоге мы получаем только измененные поля, можно их патчить
    );
    let contactId = this.getContactIndex(id); // получаем id контакта в массиве контактов
    Object.keys(receivedContact).map(field => {
      this.phoneBook[contactId][field] = receivedContact[field];
    });
  }

  editContact(id: number) {
    let contact = this.phoneBook.find(contact => contact.id === id);
    this.editingContact.next(contact);
  }
}
